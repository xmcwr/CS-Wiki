---
title: 删除排序链表中的重复元素
date: 2022-07-01 12:08:33
permalink: /pages/0ab4ce/
---
## 📃 题目描述

题目链接：[83. 删除排序链表中的重复元素](https://leetcode.cn/problems/remove-duplicates-from-sorted-list/)

![](https://cs-wiki.oss-cn-shanghai.aliyuncs.com/img/image-20220701120905538.png)

## 🔔 解题思路

使用两个指针，**一个指针指向某段重复元素的第一个节点，另一个指针用来扫描重复元素**。当遇到重复出现的节点时，直接遍历到该重复节点的末尾，然后删除两个指针中间这一段重复节点。

本题不需要头节点，因为根据题意，第一个节点是不可能被删除的


```java
class Solution {
    public ListNode deleteDuplicates(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }

        // 不需要头节点，因为根据题意，第一个节点是不可能被删除的

        // 指向重复元素的第一个节点
        ListNode pre = head;
        // 扫描重复元素
        ListNode cur = head.next;

        while (cur != null) {
            // 遇到重复出现的节点
            if (pre.val == cur.val) {
                // 循环遍历到该重复节点的末尾的下一个元素
                while (cur != null && cur.val == pre.val) {
                    cur = cur.next;
                }
                // 删除中间这一段重复节点
                pre.next = cur;
            }
            
            // 删完某段重复节点后也要更新 pre, 因为题目是要求保留重复元素的第一个元素的
            // 比如 1 3 3 4 4, pre = 1, 删完 3 后，cur = 4，更新 pre 为 cur = 4，是合题意的（保留重复元素的第一个元素）
            pre = cur;
            if (cur != null) {
                cur = cur.next;
            }
        }

        return head;
    }
}
```

## 💥 复杂度分析

- 空间复杂度：
- 时间复杂度：